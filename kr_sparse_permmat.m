function C = kr_sparse_permmat(A,B)
% Khatri-Rao product for matrices containing one 1 per column
assert(issparse(A));
assert(issparse(B));

[i1,~] = find(A);
[i2,~] = find(B);
ind = (i1-1)*size(B,1)+i2;
C = sparse(ind, 1:size(A,2), ones(size(A,2),1));
end

