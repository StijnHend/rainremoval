function [B, R] = remove_rain(O, maxiter, alpha, ml_singval_fractions, beta_mlsvd, save_iters)
% This function executes the rain removal algorithm. The arguments of this
% function are:
% - O: The Y channel of the original video.
% - maxiter: The amount of iterations of the algorithm that are executed.
% - alpha: A vector of length 4 that contains the weights of the different
%   terms in the cost function.
% - ml_singval_fractions: A vector of length 3 with values between 0 and 1.
%   These values determine the (three) dimensions of the core tensor of the 
%   MLSVD.
% - beta_mlsvd: Determines the importance of the MLSVD compared to the
%   other terms in the cost function. beta_mlsvd > 1: more important, 
%   beta_mlsvd < 1: less important.
% - save_iters: A boolean. If true, the result after each iteration is
%   saved in the folder iterations.

% Default parameter values
if nargin < 2
    maxiter = 10;
end
if nargin < 3 | isempty(alpha)
    alpha = [0.0001 0.0001 0.0001 0.0001];
else
    assert(length(alpha)==4, 'alpha should be a vector of length 4.');
end
if  nargin < 4 | isempty(ml_singval_fractions)
    ml_singval_fractions = [0.8 0.8 0.8];
else
    assert(length(ml_singval_fractions) == 3, 'ml_singval_fractions should be a vector of length 3.');
end
if nargin < 5 | isempty(beta_mlsvd)
    beta_mlsvd = 0.2;
end
if nargin < 6
    save_iters = false;
end


if save_iters
    if ~exist('iterations', 'dir')
        mkdir('iterations')
    end
    save_video_Y('iterations/original',O);
end

% Parameters
dim = size(O);
H = dim(1); W = dim(2); T = dim(3);
mu = 1;

% Initialize Lagrange multipliers
D1 = zeros(dim);
D2 = zeros(dim);
D3 = zeros(dim);
D4 = zeros(dim);
D5 = zeros(dim);

% Calculate B matrices
[B1, B2, B3] = calculate_Bmatrices(dim);
C = B1'*B1 + B2'*B2 + B3'*B3 + speye(H*W*T);

% Start optimization
disp('Rain removal optimization algorithm started');
R = zeros(dim);
iter = 1;
while iter <= maxiter
    tic;
    
    % Vi subproblems
%     V1 = shrink(V1 + D1, alpha(1));
%     V2 = shrink(V2 + D2, alpha(2));
%     V3 = shrink(V3 + D3, alpha(3));
%     V4 = shrink(V4 + D4, alpha(4));
    V1 = shrink(finite_diff(R, 1) + D1, alpha(1));
    V2 = shrink(R + D2, alpha(2));
    V3 = shrink(finite_diff(O-R, 2) + D3, alpha(3));
    V4 = shrink(finite_diff(O-R, 3) + D4, alpha(4));
    
    % L subproblem
    % determine core size
    if iter == 1
        [~, ~, sv] = mlsvd(O);
        for mode = 1:3
            for i = 1:dim(mode)
                if sum(sv{mode}(1:i))/sum(sv{mode}) > ml_singval_fractions(mode)
                    break
                end
            end
            core_dim(mode) = i;
        end
    end
    
    [Uhat,Shat,~] = mlsvd(O-R+D5, core_dim);
    L = lmlragen(Uhat,Shat);
    L(L > 1) = 1;
    L(L < 0) = 0;
    
    % R subproblem
    b = (-B1'*(D1(:)-V1(:)) - D2(:)+V2(:) + B2'*(B2*O(:)+D3(:)-V3(:)) ...
        + B3'*(B3*O(:)+D4(:)-V4(:)) - beta_mlsvd*(O(:)-L(:)+D5(:)));
    [r,~,relres] = pcg(C,b);
    assert(relres < 10^(-3));
    R = vec2tens(r, dim);
    ind = (R > O);
    R(ind) = O(ind);
    R(R < 0) = 0;
    
    % Lagrange multipliers
    D1 = D1 + finite_diff(R,1) - V1;
    D2 = D2 + R - V2;
    D3 = D3 + finite_diff(O-R,2) - V3;
    D4 = D4 + finite_diff(O-R,3) - V4;
    D5 = D5 + O-R-L;
    
    % End of an iteration
    fprintf('Iteration %d ended, runtime: %f \n', iter, toc);
    if save_iters
        save_video_Y(sprintf('iterations/iter_%d', iter), O-R);
    end
    
    iter = iter + 1;
end

B = O-R;
    