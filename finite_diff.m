function dT = finite_diff(T,mode)
    dim = size(T);
    dT = zeros(dim);
    if mode == 1
        dT(1, :, :) = T(1,:,:) - T(end,:,:);
        dT(2:end,:,:) = diff(T,1,1);
    elseif mode == 2
        dT(:, 1, :) = T(:,1,:) - T(:,end,:);
        dT(:,2:end,:) = diff(T,1,2);
    elseif mode == 3
        dT(:, :, 1) = T(:,:,1) - T(:,:,end);
        dT(:,:,2:end) = diff(T,1,3);
    else
        disp('Invalid mode, mode is 1,2 or 3.');
    end
end

