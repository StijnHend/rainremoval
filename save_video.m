function save_video(filename,Yopt,U,V,identifier)
% Saves a video in color as filename.
if nargin < 5
    identifier = '';
end
dim = size(Yopt);

R = Yopt + 1.14*V;
R(R > 1) = 1;
R(R < 0) = 0;
G = Yopt - 0.395*U - 0.581*V;
G(G > 1) = 1;
G(G < 0) = 0;
B = Yopt + 2.033*U;
B(B > 1) = 1;
B(B < 0) = 0;

video = zeros([dim(1), dim(2), 3, dim(3)]);
video(:,:,1,:) = R;
video(:,:,2,:) = G;
video(:,:,3,:) = B;

tic;
v2 = VideoWriter(strcat(filename, identifier),'MPEG-4');
open(v2);
writeVideo(v2, video);
close(v2);
fprintf('Done saving, elapsed time: %f \n', toc);
end

