clc;
% Parameters
nb_frames = 200;
maxiter=3;
alpha = [1 0.01 1 1 ];
ml_singval_fractions = [0.8 0.8 0.5];
startY = 250;
height = 200;
startX = 80;
width = 200;
beta_mlsvd = 0.2;

% Read video
[Y,U,V] = read_video('yard', nb_frames);
Y = Y(startY:startY+height-1, startX:startX+width-1,:);
U = U(startY:startY+height-1, startX:startX+width-1,:);
V = V(startY:startY+height-1, startX:startX+width-1,:);
save_video_Y('yard_original_Y', Y);

% Apply algorithm
[B, R] = remove_rain(Y, maxiter,alpha,ml_singval_fractions, beta_mlsvd);

% Save results in color
save_video('yard_rainremoved',B,U,V);

% Save results in grayscale
save_video_Y('yard_rainremoved_Y', B);
combine_videos_grayscale('yard_original_Y', 'yard_rainremoved_Y', ...
    'yard_original_and_rainremoved_Y');