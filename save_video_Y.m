function save_video_Y(filename,Y,identifier)
% Saves grayscale video Y as filename. Optional argument identifier is
% concatenated to this filename.
    if nargin < 3
        identifier = '';
    end
    video(:,:,1,:) = Y;
    tic;
    v2 = VideoWriter(strcat(filename,identifier),'MPEG-4');
    open(v2);
    writeVideo(v2, video);
    close(v2);
    fprintf('Done saving, elapsed time: %f \n', toc);
end

