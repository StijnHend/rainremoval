function [Y, U, V] = read_video(filename, nb_frames, is_grayscale)
% Reads a video file and converts it to the YUV format..
v = VideoReader(strcat(filename, '.mp4'));
if nargin < 2
    nb_frames = floor(v.framerate*v.duration);
end
if nargin < 3
    is_grayscale = false;
end
if is_grayscale
    colordim = 1;
else
    colordim = 3;
end

video_tensor = zeros(v.height, v.width, colordim, nb_frames);
i = 1;
tic;
if ~is_grayscale
    while hasFrame(v) && i <= nb_frames
        video_tensor(:,:,:,i) = readFrame(v);
        i = i+1;
    end
else
    while hasFrame(v) && i <= nb_frames
        frame = readFrame(v);
        video_tensor(:,:,:,i) = frame(:,:,1);
        i = i+1;
    end
end
fprintf('Done reading, elapsed time: %f \n', toc);

% Convert to YUV
if ~is_grayscale
    Wr = 0.299; Wg = 0.587; Wb = 0.114;
    R = squeeze(video_tensor(:,:,1,:))/255;
    G = squeeze(video_tensor(:,:,2,:))/255;
    B = squeeze(video_tensor(:,:,3,:))/255;
    Y = Wr*R + Wg*G + Wb*B;

    Umax = 0.436; Vmax = 0.615;
    U = Umax/(1-Wb) * (B-Y);
    V = Vmax/(1-Wr) * (R-Y);
else
    Y = squeeze(video_tensor);
end

% save(strcat(filename, '.mat'), 'video_tensor');
end