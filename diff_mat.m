function D = diff_mat(dim)
%     D = diag(ones(dim,1)) - diag(ones(dim-1,1),-1);
%     D(1,dim)= -1;
    
    i1 = [1:dim 2:dim 1];
    i2 = [1:dim 1:dim-1 dim];
    val = [ones(1, dim), -ones(1,dim)];
    D = sparse(i1, i2, val);
end

