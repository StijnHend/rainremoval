% Fast inversion for B and R tensors in fastderain
% ------------------------------------------------
%
% We compare some algorithms to compute the inversion step when computing B or R. The trick is to
% write the sum of two (three) Kronecker products as a sum of one (two) Kronecker products, which
% is possible for the structure at hand: kron(I,M) + kron(I,I) = kron(I,M+I). Next, we exploit
% this structure by using the right reshape operator, or by casting the problem into a Sylvester
% equation. 

% Author(s): Nico Vervliet
%
% Version History:
% - 2019/11/21   NV      Initial version

% Just some random tensor
I = 512;
J = 512;
K = 100;
T = randn(I,J,K);

% Some convenient functions 
Delta = @(n) spdiags(ones(n,3).*[-1 2 -1], -1:1, n, n);
vec   = @(x) reshape(x, [], 1);
unvec = @(x) reshape(x, [I J K]);

% The displacement operators
D1 = Delta(I) + speye(I);
D2 = Delta(J);
D3 = Delta(K);

%% Target
if I*J*K < 1e3
    M = kron(eye(K),eye(J),D1) + kron(eye(K),D2,eye(I)) + kron(D3,eye(J),eye(I));
    tic 
    Xt = unvec(M \ T(:));
    toc
end 

%% Bartels-Stewart
[QA,a] = eig(full(D1),'vector');
[QB,b] = eig(full(D2),'vector');
[QC,c] = eig(full(D3),'vector');

tic 
Xs = tmprod(T, {QA,QB,QC}, 1:3, 'T');
Xs = Xs ./ (a + b' + reshape(c, [1 1 K]));
Xs = tmprod(Xs, {QA,QB,QC}, 1:3);
toc 

% if all(size(Xs) == size(Xt))
%     frob(Xt-Xs)/frob(Xt)
% end 

%% Fewer permutations
tic 
Xp = reshape(reshape(QA'*reshape(T,I,J*K),I*J,K)*QC,I,J,K);
Xp = reshape(reshape(permute(Xp, [1 3 2]),I*K,J)*QB,I,K,J);
Xp = Xp ./ (a + c' + reshape(b, [1 1 J]));
Xp = reshape(reshape(QA*reshape(Xp,I,K*J),I*K,J)*QB',I,K,J);
Xp = reshape(reshape(permute(Xp, [1 3 2]),I*J,K)*QC',I,J,K);
toc 

frob(Xp-Xs)/frob(Xs)