% Fast inversion for B and R tensors in fastderain
% ------------------------------------------------
%
% We compare some algorithms to compute the inversion step when computing B or R. The trick is to
% write the sum of two (three) Kronecker products as a sum of one (two) Kronecker products, which
% is possible for the structure at hand: kron(I,M) + kron(I,I) = kron(I,M+I). Next, we exploit
% this structure by using the right reshape operator, or by casting the problem into a Sylvester
% equation. 

% Author(s): Nico Vervliet
%
% Version History:
% - 2019/11/21   NV      Initial version

% Just some random tensor
I = 512;
J = 512;
K = 100;
T = randn(I,J,K);

% Some convenient functions 
Delta = @(n) spdiags(ones(n,3).*[-1 2 -1], -1:1, n, n);
vec   = @(x) reshape(x, [], 1);
unvec = @(x) reshape(x, [I J K]);

% The displacement operators
D1 = Delta(I) + speye(I);
D2 = Delta(J) + speye(J);
D3 = Delta(K);

%% Solving for R
disp('Solving for R');

%% Target solution
%  The slow way.
if I*J*K < 1e4
    M = kron(eye(J*K),D1);
    fprintf('%-15s', 'slow');
    tic 
    Xt = reshape(M\vec(T),I,J,K);
    toc
end

%% Fast solution
%  Fast solution exploits Kronecker structure. This can be made faster by factorizing or
%  inverting D first, i.e., we can spread out this cost over multiple updates.
fprintf('%-15s', 'fast');
tic
Xf = unvec(D1 \ reshape(T,I,[]));
toc 

if I*J*K < 1e4, frob(Xt-Xf)/frob(Xt), end

%% Solving for B
disp('Solving for B');

%% Standard solution
%  This method simply inverts the matrix M and is very slow. 
if I*J*K < 1e3
    fprintf('%-15s', 'slow');
    M = kron(eye(K),D2,eye(I)) + kron(D3,eye(J*I));
    % Target solution
    tic 
    Xt = reshape(M\vec(T),I,J,K);
    toc
end 

%% Semi fast solution
%  We exploit the fact that we have a sum of two Kronecker products, in each of which one matrix
%  is identity. This way, we can convert this problem to solving a
%  Sylvester equation D2*X + X*D2 (3?)
%  = T(i,:,:). Each X is a horizontal (mode-1) slice of the solution.
fprintf('%-15s', 'semi-fast');
tic
Xs = zeros(I,J,K);
for i = 1:I
    Xs(i,:,:) = sylvester(full(D2),full(D3),squeeze(T(i,:,:)));
end
toc 

%% Precompute some reults
%  Sylvester for Hermitian matrices uses the EVD and some simple steps. By precomputing some
%  results, we can speed up computations.
%
%  Timed seperately as it is a one time cost. 
fprintf('%-15s', 'prepping');
tic 
[QA, dA] = eig(full(D2), 'vector'); 
[QB, dB] = eig(full(D3), 'vector'); 
toc 

%% Faster solution
%  We need QA and QB.
%  Sylvester for Hermitian matrices uses the EVD and some simple steps. We can reuse a lot of
%  results and update all slices at the same time, removing the need for for-loops.
fprintf('%-15s', 'faster');
tic 
Xf = tmprod(T, {QA, QB}, [2,3], 'H'); 
Xf = Xf ./ (reshape(dA,[1 J 1]) + reshape(dB, [1 1 K]));
Xf = tmprod(Xf, {QA, QB}, [2 3]);
toc 

%% Superfast solution
%  We need QA and QB.
%  We now prepermute the tensor first. This saves halve of the permutations.
fprintf('%-15s', 'superfast');
tic 
Xff = permute(T, [2 1 3]);
Xff = reshape(reshape(QA'*reshape(Xff,J,I*K),J*I,K)*QB, [J,I,K]);
Xff = Xff ./ (reshape(dA,[J 1 1]) + reshape(dB, [1 1 K]));
Xff = reshape(reshape(QA*reshape(Xff,J,I*K),J*I,K)*QB', [J,I,K]);
Xff = permute(Xff, [2 1 3]);
toc 

if I*J*K < 1e3, 
    frob(Xt-Xs)/frob(Xt)
    frob(Xt-Xf)/frob(Xt)
    frob(Xt-Xff)/frob(Xt)
end

% % Fast solution
% tic
% Xf = unvec(D \ reshape(T,I,[]));
% toc 


% DD = zeros(J);
% DD(1,1) = D2(1,1);
% for n = 2:J
%     DD(n,n) = D2(n,n) - 1/DD(n-1,n-1);
% end
% LL = DD + diag(diag(D2,-1),-1)

