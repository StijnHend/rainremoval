function [B1, B2, B3] = calculate_Bmatrices(dim)
    H = dim(1); W = dim(2); T = dim(3);
    
    B1 = kron(speye(W*T), diff_mat(H));
    B2 = kron(speye(T), diff_mat(W), speye(H));
    B3 = kron(diff_mat(T), speye(H*W));
end

