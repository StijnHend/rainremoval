function combine_videos_grayscale(sourcefile1,sourcefile2,resultfile)
% Combines two video files into one. Sourcefile1 is shown on the left and
% sourcefile2 on the right.
V=read_video(sourcefile1);
V1(:,:,1,:)=V;
V=read_video(sourcefile2);
V2(:,:,1,:)=V;
assert(boolean(prod(size(V1)==size(V2))), 'Videos should have the same resolution and number of frames.');
dim=size(V1);
result=zeros([dim(1) dim(2)*2 1 dim(4)]);
result(:,1:dim(2),:,:) = V1;
result(:,dim(2)+1:2*dim(2),:,:) = V2;
save_video_Y(resultfile, result);
end

